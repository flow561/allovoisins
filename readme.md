#Allovoisins test

##demo
http://florentsimon.io/allovoisins/allovoisins.html

##Required
- php server running on localhost:8080

##Installation
```
yarn install / npm i
gulp watch
```

##Structure
- _reset.scss : Reset and transform existing components
- _global.scss : Style for non-existing components

##Work achieved
- Rebuild home searchbar
- Add searchbar animation on keyup
- Rebuild menu
- Add Signup animation
- Rebuild mobile menu from side panel with css animation to fullscreen with gsap
- Rebuild mobile menu trigger
- Cleaning last searchs structure
- Fixing h1/h2/h3 structure

##TODO
- Logo animation
- Scroll animation
- Press container hover
- Social links hover
- Footer links hover
- Global font size
- Title styles