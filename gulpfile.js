var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssclean = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();

var jspaths = [
    './node_modules/gsap/jquery.gsap.js',
    './node_modules/gsap/TimelineMax.js',
    './node_modules/gsap/TweenMax.js',
    './florent/src/js/**'
];


gulp.task('css', function(){
    return gulp.src('florent/src/scss/style.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('florent/assets/css'))
        .pipe(browserSync.stream());
});


gulp.task ('js', function(){
    return gulp.src(jspaths)
        .pipe(plumber())
        .pipe(concat('app.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('florent/assets/js'))
        .pipe(browserSync.stream());
});

gulp.task('html', function(){
    return gulp.src('./*.html')
        .pipe(plumber())
        .pipe(browserSync.stream());
});

gulp.task('browserSync', function() {
    browserSync.init({
        proxy: 'http://127.0.0.1:8080/allovoisins.html'
    });
});

gulp.task('build', ['css', 'js']);

gulp.task('watch', ['browserSync', 'js', 'css', 'html'], function (){
    gulp.watch('florent/src/scss/**/*.scss', ['css']);
    gulp.watch('florent/src/js/*.js', ['js']);
    gulp.watch('./*.html', ['html']);
});