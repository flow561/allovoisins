$(document).ready(function(){

    // Home searchbar
    function searchBar(){
        var input = $('#home-search-input');
        var nextInputs = $('.home-search .bottom');
        var ctgTrigger = $('.category-trigger');
        var ctg = $('.categories');
        var ctgLinks = $('.categories a');
        var ctgStat = 0;
        TweenLite.set(nextInputs, {y: '-100%'})
        input.on('keyup', function(){
            if(input.val() !== ''){
                TweenLite.to(nextInputs, 0.4, {y: '0%'})
            }else{
                TweenLite.to(nextInputs, 0.4, {y: '-100%'})
                TweenLite.to(ctg, 0.3, {autoAlpha: 0, y: 30, ease: Power2.easeOut})
                ctgStat = 0;
            }
        });
        TweenLite.set(ctg, {autoAlpha: 0, y: 30})
        ctgTrigger.on('click', function(e){
            if(ctgStat == 0){
                ctgStat = 1;
                TweenLite.to(ctg, 0.3, {autoAlpha: 1, y: 0, ease: Power2.easeOut})
            }else{
                ctgStat = 0;
                TweenLite.to(ctg, 0.3, {autoAlpha: 0, y: 30, ease: Power2.easeOut})
            }

        });
    }
    searchBar();

    //Signup button
    function signupButton(){
        var button = $('.signin');
        var illu = button.find('img');
        TweenLite.set(illu, {transformOrigin: 'center bottom'})
        button.on('mouseenter', function(){
            console.log('ok');
            var tl = new TimelineLite();
            tl.to(illu, 0.1, {rotationZ: '-20deg'})
            tl.to(illu, 0.1, {rotationZ: '10deg'})
            tl.to(illu, 0.1, {rotationZ: '-10deg'})
            tl.to(illu, 0.1, {rotationZ: '5deg'})
            tl.to(illu, 0.1, {rotationZ: '-5deg'})
            tl.to(illu, 0.1, {rotationZ: '0deg'})
        })
    }
    signupButton();

    //Mobile menu
    function mobileMenu(){
        var menu = $('#menubar__mobileNavDropdown');
        var trigger = $('#menubar__mobileActionDropdown')
        var links = menu.find('a');

        var state = 0;

        TweenLite.set(menu, {y: '-100%'})
        TweenLite.set(links, {autoAlpha: 0, x: -20})
        trigger.on('click', function(){
            if(state == 0){
                state = 1;
                TweenLite.to(menu, 0.6, {y: '0%', ease: Power2.easeOut})
                TweenMax.staggerTo(links, 0.4, {autoAlpha: 1, x: 0}, 0.01)
            }else{
                state = 0;
                TweenLite.to(links, 0.4, {autoAlpha: 0})
                TweenLite.to(menu, 0.6, {y: '-100%', ease: Power2.easeOut})
            }
        })
    }
    mobileMenu();

})